package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"sync"
	"time"
)

var timeout uint
var network string
var address string

func init() {
	flag.UintVar(&timeout, "timeout", 0, "connection timeout")
	flag.StringVar(&network, "network", "tcp", "network")
	flag.StringVar(&address, "address", "", "address")
}

func main() {
	flag.Parse()
	dialer := &net.Dialer{}
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Duration(timeout)*time.Millisecond)
	conn, err := dialer.DialContext(ctx, network, address)
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}

	wg := sync.WaitGroup{}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	go func() {
		<-c
		log.Fatalf("Get SIGINT signal")
	}()

	wg.Add(1)
	go func() {
		readRoutine(ctx, conn)
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		writeRoutine(ctx, conn)
		wg.Done()
	}()

	wg.Wait()

	cancel()

	conn.Close()
}

func readRoutine(ctx context.Context, conn net.Conn) {
INNER:
	for {
		select {
		case <-ctx.Done():
			break INNER
		default:
			timeoutDuration := time.Millisecond * 100
			bufReader := bufio.NewReader(conn)

			for {
				// Set a deadline for reading. Read operation will fail if no data
				// is received after deadline.
				conn.SetReadDeadline(time.Now().Add(timeoutDuration))

				// Read tokens delimited by newline
				bytes, err := bufReader.ReadBytes('\n')
				if err != nil {
					continue INNER
				}

				fmt.Printf("From server: %s", bytes)
			}
		}
	}
	log.Printf("Finished readRoutine")
}

func writeRoutine(ctx context.Context, conn net.Conn) {
	input := make(chan string, 1)
	go getInput(input)
OUTER:
	for {
		select {
		case <-ctx.Done():
			break OUTER
		case i := <-input:
			log.Printf("To server %v\n", i)

			_, err := conn.Write([]byte(fmt.Sprintf("%s\n", i)))

			if err != nil {
				log.Printf("cannot write to connection: %v", err)
			}
		}
	}

	log.Printf("Finished writeRoutine")
}

func getInput(input chan string) {
	for {
		in := bufio.NewReader(os.Stdin)
		result, err := in.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		input <- result
	}
}
